const functions = require('firebase-functions');
const admin = require('firebase-admin');

admin.initializeApp();

exports.addEmail = functions.https.onCall((data, context) => {
  const store = admin.firestore();
  return store.collection('mail').add(data)
  .then(ref => {
    return store.collection('insurance').doc('mRipLS7sAJvLfOzpNb45')
      .update({ mail: `/mail/${ref.id}` })
  }).then(() => {
    return { success: true }
  }).catch((err) => {
    return { success: false, error: err }
  });
});

