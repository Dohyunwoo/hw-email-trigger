import Vue from 'vue'
import VueCompositionApi, { reactive, computed } from '@vue/composition-api'

Vue.use(VueCompositionApi) 

const email = reactive({
  address: null,
  submitFlag: false,
  mailForm(address) {
    const form = {
      to: address,
      message: {
        subject: '테스트 이메일입니다.',
        text: '테스트 전송입니다.',
        html: '<h1>테스트 전송입니다.</h1>'
      }
    }
    return form
  },
  sendEmail(address) {
    if(email.submitFlag) return alert('전송중입니다.')
    else email.submitFlag = true

    const ref = firebase.functions().httpsCallable('addEmail')
    ref(email.mailForm(address)).then((r) => {
      if(r.data.success) alert('메일 송신 완료')
      else alert('메일 송신 실패')
    }).catch(err => {
      alert(`firebase 통신 실패 : ${err}`)
    }).finally(() => email.submitFlag = false)
  }
})

export default email
