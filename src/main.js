import Vue from 'vue'
import VueCompositionApi from '@vue/composition-api'
import VueRouter from 'vue-router'
import router from '@/router'
import '@/registerServiceWorker'

Vue.config.productionTip = false
Vue.use(VueCompositionApi)
Vue.use(VueRouter)

new Vue({
    router,
    render: h => <router-view/>
}).$mount('#app')
